// This file is part of mtemu. See toplevel LICENSE file for license.

#pragma once

#include <libevdev/libevdev.h>

#include <cstdint>
#include <memory>
#include <string>

void EvdevCheckReturnValue(int ret, std::string const& what);

class Evdev
{
  std::shared_ptr<libevdev> m_dev;

  struct EvdevDeleter
  {
    void operator()(libevdev* dev);
  };
public:
  Evdev();
  Evdev(std::string const& path);
  int fd();
  void grab(libevdev_grab_mode grab);
  std::string name();
  void enableEventType(unsigned int type);
  void enableEventCode(unsigned int type, unsigned int code, const void* data = nullptr);
  void enableEventCodes(unsigned int type, std::initializer_list<unsigned int> codes);
  void enableAbsCode(unsigned int code, std::int32_t min, std::int32_t max, std::int32_t resolution = 0, std::int32_t value = 0, std::int32_t fuzz = 0, std::int32_t flat = 0);
  void enableProperty(unsigned int prop);
  operator libevdev*();
};