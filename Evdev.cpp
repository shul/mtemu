// This file is part of mtemu. See toplevel LICENSE file for license.

#include "Evdev.h"

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include <cassert>
#include <cerrno>
#include <system_error>

void EvdevCheckReturnValue(int ret, const std::string& what)
{
  if (ret != 0)
  {
    throw std::system_error(-ret, std::generic_category(), what);
  }
}

void Evdev::EvdevDeleter::operator()(libevdev* dev)
{
  int fd = libevdev_get_fd(dev);
  if (fd > 0)
  {
    close(fd);
  }
  libevdev_free(dev);
}

Evdev::Evdev()
: m_dev(libevdev_new(), EvdevDeleter())
{
  assert(m_dev);
}

Evdev::Evdev(std::string const& path)
{
  int fd = open(path.c_str(), O_RDONLY | O_NONBLOCK);
  if (fd < 0)
  {
    throw std::system_error(errno, std::generic_category(), std::string("open ") + path);
  }
  libevdev* dev;
  try
  {
    EvdevCheckReturnValue(libevdev_new_from_fd(fd, &dev), "libevdev_new_from_fd");
  }
  catch(...)
  {
    close(fd);
    throw;
  }
  assert(dev);
  m_dev.reset(dev, EvdevDeleter());
}

int Evdev::fd()
{
  return libevdev_get_fd(m_dev.get());
}

std::string Evdev::name()
{
  return libevdev_get_name(m_dev.get());
}

Evdev::operator libevdev*()
{
  return m_dev.get();
}

void Evdev::grab(libevdev_grab_mode grab)
{
  EvdevCheckReturnValue(libevdev_grab(m_dev.get(), grab), "libevdev_grab");
}

void Evdev::enableEventType(unsigned int type)
{
  EvdevCheckReturnValue(libevdev_enable_event_type(m_dev.get(), type), "libevdev_enable_event_type");
}

void Evdev::enableEventCode(unsigned int type, unsigned int code, const void* data)
{
  EvdevCheckReturnValue(libevdev_enable_event_code(m_dev.get(), type, code, data), "libevdev_enable_event_code");
}

void Evdev::enableEventCodes(unsigned int type, std::initializer_list<unsigned int> codes)
{
  for (auto code : codes)
  {
    enableEventCode(type, code);
  }
}

void Evdev::enableAbsCode(unsigned int code, std::int32_t min, std::int32_t max, std::int32_t resolution, std::int32_t value, std::int32_t fuzz, std::int32_t flat)
{
  // Clamp initial value e.g. if it was not given
  if (value < min)
  {
    value = min;
  }
  if (value > max)
  {
    value = max;
  }

  input_absinfo absinfo =
  {
    .value = value,
    .minimum = min,
    .maximum = max,
    .fuzz = fuzz,
    .flat = flat,
    .resolution = resolution
  };
  enableEventCode(EV_ABS, code, &absinfo);
}

void Evdev::enableProperty(unsigned int prop)
{
  EvdevCheckReturnValue(libevdev_enable_property(m_dev.get(), prop), "libevdev_enable_property");
}