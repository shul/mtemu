// This file is part of mtemu. See toplevel LICENSE file for license.

#pragma once

#include <libevdev/libevdev-uinput.h>

#include <memory>
#include <string>

#include "Evdev.h"

class EvdevUInput
{
  std::shared_ptr<libevdev_uinput> m_dev;
public:
  EvdevUInput();
  explicit EvdevUInput(Evdev& evdev);
  int fd();
  std::string syspath();
  std::string devnode();
  void writeEvent(unsigned int type, unsigned int code, int value);
  libevdev_uinput* cPtr();
  operator libevdev_uinput*();
};