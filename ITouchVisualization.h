// This file is part of mtemu. See toplevel LICENSE file for license.

#pragma once

class ITouchVisualization
{
public:
  virtual ~ITouchVisualization() {}
  virtual void showHoverAt(unsigned int slot, double x, double y, double size) = 0;
};