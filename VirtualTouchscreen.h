// This file is part of mtemu. See toplevel LICENSE file for license.

#pragma once

#include <cstdint>
#include <map>
#include <queue>

#include "EvdevUInput.h"
#include "ITouchVisualization.h"

class VirtualTouchscreen
{
  static constexpr int ABS_RESOLUTION_X{8192};
  static constexpr int ABS_RESOLUTION_Y{8192};
  static constexpr int DEFAULT_TOUCH_SIZE{256};

  static constexpr std::uint16_t EV_CUSTOM{EV_CNT};
  enum CustomEventCode : std::uint16_t
  {
    CUSTOM_EVENT_TOUCH_DOWN = 0,
    CUSTOM_EVENT_TOUCH_UP
  };

  // like input_event but without time
  struct Event
  {
    std::uint16_t type;
    std::uint16_t code;
    std::int32_t value;
    Event(std::uint16_t type, std::uint16_t code, std::int32_t value = 0);
  };

  struct Source
  {
    Evdev device;
    std::int32_t slot;

    std::int32_t x{0}, y{0};
    std::int32_t size{DEFAULT_TOUCH_SIZE};

    std::queue<Event> eventQueue;

    Source(Evdev& device, std::int32_t slot);
  };

  // Map of fd number to device
  std::map<int, Source> m_sources;
  EvdevUInput m_target;
  std::int32_t m_trackingId{0};
  int m_currentSlot{-1};
  int m_activeTouchPoints{0};
  unsigned int m_scaleFactor{1};

  ITouchVisualization* m_visualization{};

  void prepareTargetDevice(Evdev& device);
  void processEvent(Source& source, input_event const& event);
  void pump(Source& source);
  void switchTouchPointCount(int from, int to);
  void updateVisualization(Source& source);

public:
  VirtualTouchscreen();

  void prepare();

  void addSourceDevice(std::string const& path);

  void run();
  void setScaleFactor(unsigned int factor);
  void setVisualization(ITouchVisualization* visualization);
};