// This file is part of mtemu. See toplevel LICENSE file for license.

#include <iostream>
#include <thread>

#include "EvdevUInput.h"
#include "QtTouchVisualizer.h"
#include "VirtualTouchscreen.h"

int main(int argc, char** argv)
{
  QtTouchVisualizer vis(argc, argv);

  if (argc < 3)
  {
    std::cerr << "Usage: " << argv[0] << " <scale factor> <mouse input device ...>" << std::endl;
    return 0;
  }

  VirtualTouchscreen emulator;

  emulator.setScaleFactor(std::stoi(argv[1]));
  emulator.setVisualization(vis.getHandler());

  for (int arg = 2; arg < argc; arg++)
  {
    emulator.addSourceDevice(argv[arg]);
  }

  emulator.prepare();
 
  std::thread eventThread([&]
  {
    emulator.run();
  });

  vis.run();
  
  return 0;
}

