CONFIG += qt debug warn_on c++11 link_pkgconfig
QT += widgets waylandclient-private
PKGCONFIG += libevdev
HEADERS += Evdev.h EvdevUInput.h ITouchVisualization.h QtTouchVisualizer.h QtTouchVisualizationWindow.h VirtualTouchscreen.h
SOURCES += main.cpp Evdev.cpp EvdevUInput.cpp QtTouchVisualizer.cpp QtTouchVisualizationWindow.cpp VirtualTouchscreen.cpp
