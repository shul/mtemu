// This file is part of mtemu. See toplevel LICENSE file for license.

#include "VirtualTouchscreen.h"

#include <poll.h>

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <list>
#include <vector>

namespace
{

template<typename T>
T Clamp(const T value, const T min, const T max)
{
  if (value < min)
  {
    return min;
  }
  else if (value > max)
  {
    return max;
  }
  else
  {
    return value;
  }
}

std::int32_t ToolForTapCount(int count)
{
  switch (count)
  {
    case 2:
      return BTN_TOOL_DOUBLETAP;
    case 3:
      return BTN_TOOL_TRIPLETAP;
    case 4:
      return BTN_TOOL_QUADTAP;
    case 5:
      return BTN_TOOL_QUINTTAP;
    default:
      return -1;
  }
}

}

constexpr int VirtualTouchscreen::ABS_RESOLUTION_X;
constexpr int VirtualTouchscreen::ABS_RESOLUTION_Y;
constexpr std::uint16_t VirtualTouchscreen::EV_CUSTOM;

VirtualTouchscreen::Event::Event(std::uint16_t type, std::uint16_t code, std::int32_t value)
: type{type}, code{code}, value{value}
{
}

VirtualTouchscreen::Source::Source(Evdev& device, std::int32_t slot)
: device{device}, slot{slot}
{
}

VirtualTouchscreen::VirtualTouchscreen()
{
}

void VirtualTouchscreen::prepareTargetDevice(Evdev& device)
{
  libevdev_set_name(device, "Virtual touchscreen");
  device.enableEventType(EV_ABS);
  // Do not report resolution, compositor can just scale as it wishes
  device.enableAbsCode(ABS_MT_POSITION_X, 0, ABS_RESOLUTION_X);
  device.enableAbsCode(ABS_MT_POSITION_Y, 0, ABS_RESOLUTION_Y);
  device.enableAbsCode(ABS_MT_TOUCH_MAJOR, 0, sqrt(ABS_RESOLUTION_X * ABS_RESOLUTION_X + ABS_RESOLUTION_Y * ABS_RESOLUTION_Y), 0, DEFAULT_TOUCH_SIZE);
  device.enableAbsCode(ABS_MT_SLOT, 0, m_sources.size() - 1);
  device.enableAbsCode(ABS_MT_TRACKING_ID, 0, std::numeric_limits<std::int32_t>::max());
  // This is somewhat arbitrary
  device.enableAbsCode(ABS_MT_PRESSURE, 0, 255);

  device.enableEventType(EV_KEY);
  device.enableEventCodes(EV_KEY, { BTN_TOUCH, BTN_TOOL_DOUBLETAP, BTN_TOOL_TRIPLETAP, BTN_TOOL_QUADTAP, BTN_TOOL_QUINTTAP });
}

void VirtualTouchscreen::addSourceDevice(const std::string& path)
{
  std::cout << "Opening " << path << std::endl;
  Evdev source(path);
  source.grab(LIBEVDEV_GRAB);

  auto slot = m_sources.size();
  m_sources.emplace(std::piecewise_construct, std::forward_as_tuple(source.fd()), std::forward_as_tuple(source, slot));
}

void VirtualTouchscreen::setScaleFactor(unsigned int factor)
{
  m_scaleFactor = factor;
}

void VirtualTouchscreen::prepare()
{
  Evdev mockDevice;
  prepareTargetDevice(mockDevice);

  std::cout << "Creating uinput device" << std::endl;
  m_target = EvdevUInput(mockDevice);
  std::cout << "Got uinput device at " << m_target.devnode() << std::endl;
}

void VirtualTouchscreen::switchTouchPointCount(int from, int to)
{
  // Only one of BTN_TOOL_* may be active, so switch between them
  if (from >= 2 && ToolForTapCount(from) != -1)
  {
    m_target.writeEvent(EV_KEY, ToolForTapCount(from), 0);
  }
  if (to >= 2 && ToolForTapCount(to) != -1)
  {
    m_target.writeEvent(EV_KEY, ToolForTapCount(to), 1);
  }

  // BTN_TOUCH is always on as long as there is at least one tap
  if (from == 1 && to < from)
  {
    m_target.writeEvent(EV_KEY, BTN_TOUCH, 0);
  }
  else if (to == 1 && to > from)
  {
    m_target.writeEvent(EV_KEY, BTN_TOUCH, 1);
  }
}

void VirtualTouchscreen::processEvent(Source& source, const input_event& event)
{
  if (libevdev_event_is_code(&event, EV_SYN, SYN_REPORT))
  {
    if (!source.eventQueue.empty())
    {
      if (m_currentSlot != source.slot)
      {
        m_target.writeEvent(EV_ABS, ABS_MT_SLOT, source.slot);
        m_currentSlot = source.slot;
      }

      // Empty queue, then also send SYN_REPORT
      while (!source.eventQueue.empty())
      {
        auto const& event = source.eventQueue.front();
        switch (event.type)
        {
          case EV_CUSTOM:
          switch (event.code)
          {
            case CUSTOM_EVENT_TOUCH_UP:
            {
              auto wasActiveTouchPoints = m_activeTouchPoints--;
              m_activeTouchPoints = std::max(0, m_activeTouchPoints);
              switchTouchPointCount(wasActiveTouchPoints, m_activeTouchPoints);

              m_target.writeEvent(EV_ABS, ABS_MT_PRESSURE, 0);
              // Touch up: Unassign tracking ID
              m_target.writeEvent(EV_ABS, ABS_MT_TRACKING_ID, -1);
            }
              break;
            case CUSTOM_EVENT_TOUCH_DOWN:
            {
              auto wasActiveTouchPoints = m_activeTouchPoints++;
              m_activeTouchPoints = std::min(static_cast<int> (m_sources.size()), m_activeTouchPoints);
              switchTouchPointCount(wasActiveTouchPoints, m_activeTouchPoints);

              // Touch down: Assign tracking ID
              m_trackingId = Clamp(m_trackingId + 1, 0, std::numeric_limits<decltype(m_trackingId)>::max());
              m_target.writeEvent(EV_ABS, ABS_MT_TRACKING_ID, m_trackingId);
              m_target.writeEvent(EV_ABS, ABS_MT_PRESSURE, 128);
            }
              break;
            default:
              throw std::runtime_error("Invalid custom event code");
          }
          break;
          
          default:
            m_target.writeEvent(event.type, event.code, event.value);
        }
        source.eventQueue.pop();
      }
      m_target.writeEvent(EV_SYN, SYN_REPORT, 0);
    }
  }
  else if (libevdev_event_is_type(&event, EV_REL))
  {
    if (libevdev_event_is_code(&event, EV_REL, REL_X))
    {
      source.x = Clamp(source.x + event.value * static_cast<int> (m_scaleFactor), 0, ABS_RESOLUTION_X);
      source.eventQueue.emplace(EV_ABS, ABS_MT_POSITION_X, source.x); //(ABS_RESOLUTION_X + source.x) / 2);
      updateVisualization(source);
    }
    else if (libevdev_event_is_code(&event, EV_REL, REL_Y))
    {
      source.y = Clamp(source.y + event.value * static_cast<int> (m_scaleFactor), 0, ABS_RESOLUTION_Y);
      source.eventQueue.emplace(EV_ABS, ABS_MT_POSITION_Y, source.y);
      updateVisualization(source);
    }
    else if (libevdev_event_is_code(&event, EV_REL, REL_WHEEL))
    {
      source.size = Clamp(source.size + event.value * static_cast<int> (m_scaleFactor), 0, static_cast<int> (sqrt(ABS_RESOLUTION_X * ABS_RESOLUTION_X + ABS_RESOLUTION_Y * ABS_RESOLUTION_Y)));
      source.eventQueue.emplace(EV_ABS, ABS_MT_TOUCH_MAJOR, source.size);
      updateVisualization(source);
    }
  }
  else if (libevdev_event_is_type(&event, EV_KEY))
  {
    if (libevdev_event_is_code(&event, EV_KEY, BTN_LEFT))
    {
      // Post custom event to the queue in order to delay tracking ID assignment
      // etc. until the SYN of the source device
      switch (event.value)
      {
        case 0:
          source.eventQueue.emplace(EV_CUSTOM, CUSTOM_EVENT_TOUCH_UP);
          break;
        case 1:
          source.eventQueue.emplace(EV_CUSTOM, CUSTOM_EVENT_TOUCH_DOWN);
          break;
        default:
          throw std::runtime_error("Invalid key value");
      }
    }
  }
}

void VirtualTouchscreen::run()
{
  std::vector<pollfd> pollfds{};
  pollfds.reserve(m_sources.size());
  std::transform(m_sources.begin(), m_sources.end(), std::back_inserter(pollfds), [](decltype(m_sources)::value_type& pair)
  {
    pollfd fd{};
    fd.fd = pair.second.device.fd();
    fd.events = POLLIN;
    return fd;
  });

  while(true)
  {
    //std::cout << "> poll" << std::endl;
    int ret = poll(pollfds.data(), pollfds.size(), -1);
    //std::cout << "< poll" << std::endl;
    if (ret < 0)
    {
      throw std::system_error(errno, std::generic_category(), "poll");
    }

    for (auto const& fd : pollfds)
    {
      if (fd.revents & POLLERR || fd.revents & POLLHUP || fd.revents & POLLNVAL)
      {
        throw std::runtime_error("poll fd had error set");
      }

      if (fd.revents & POLLIN)
      {
        pump(m_sources.at(fd.fd));
      }
    }
  }
}

void VirtualTouchscreen::pump(Source& source)
{
  int ret;
  bool isSync = false;
  do
  {
    input_event ev;
    ret = libevdev_next_event(source.device, isSync ? LIBEVDEV_READ_FLAG_SYNC : LIBEVDEV_READ_FLAG_NORMAL, &ev);
    if (ret == LIBEVDEV_READ_STATUS_SYNC && !isSync)
    {
      // SYN_DROPPED, start sync
      isSync = true;
      processEvent(source, ev);
    }
    else if (ret == -EAGAIN && isSync)
    {
      // Sync ended, try again
      isSync = false;
      // Continue even if -EAGAIN was received
      continue;
    }
    else if (ret == LIBEVDEV_READ_STATUS_SUCCESS || ret == LIBEVDEV_READ_STATUS_SYNC)
    {
      // Usual case
      processEvent(source, ev);
    }
    else if (ret != -EAGAIN)
    {
      // Other error occurred
      throw std::system_error(-ret, std::generic_category(), "libevdev_next_event");
    }
  } while(ret != -EAGAIN);
}

void VirtualTouchscreen::setVisualization(ITouchVisualization* visualization)
{
  m_visualization = visualization;
}

void VirtualTouchscreen::updateVisualization(Source& source)
{
  if (m_visualization)
  {
    m_visualization->showHoverAt(source.slot, static_cast<double> (source.x) / ABS_RESOLUTION_X, static_cast<double> (source.y) / ABS_RESOLUTION_Y, static_cast<double> (source.size) / (ABS_RESOLUTION_X + ABS_RESOLUTION_Y) / 2.0);
  }
}
