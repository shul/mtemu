// This file is part of mtemu. See toplevel LICENSE file for license.

#include "QtTouchVisualizer.h"

#include <QApplication>

QtTouchVisualizer::QtTouchVisualizer(int& argc, char**& argv)
: m_app(argc, argv)
{
}

ITouchVisualization* QtTouchVisualizer::getHandler()
{
  return &m_window;
}

void QtTouchVisualizer::run()
{
  m_app.exec();
}
