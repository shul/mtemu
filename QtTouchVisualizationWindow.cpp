// This file is part of mtemu. See toplevel LICENSE file for license.

#include "QtTouchVisualizationWindow.h"

#include <iostream>

/*
 * XXX
 * Qt 5.9 supports xdg_shell_unstable_v6, but is not able to make windows
 * full screen. All this hackery is just for that purpose.
 * #define private public is needed to get the zxdg_toplevel_v6 from the
 * shell surface.
 */
#include <QtGui/private/qwindow_p.h>
#include <QtWaylandClient/private/qwaylandscreen_p.h>
#include <QtWaylandClient/private/qwaylandwindow_p.h>
#define private public
#include <QtWaylandClient/private/qwaylandxdgshellv6_p.h>
#undef private

#include <QApplication>
#include <QWindow>
#include <QPainter>

QtTouchVisualizationWindow::QtTouchVisualizationWindow()
: QWidget(nullptr, Qt::Window | Qt::CustomizeWindowHint | Qt::FramelessWindowHint)
{
  setAttribute(Qt::WA_NoSystemBackground, true);
  setAttribute(Qt::WA_TranslucentBackground, true);
  // This does not make it full screen :-(
  showFullScreen();
  /*
  if (qApp->screens().size() > 1)
  {
    // Show on second screen
    windowHandle()->setScreen(qApp->screens()[1]);
  }
  */
}

void QtTouchVisualizationWindow::showHoverAt(unsigned int slot, double x, double y, double size)
{
  {
    std::lock_guard<decltype(m_pointsMutex)> lock(m_pointsMutex);

    if (slot >= m_points.size())
    {
      m_points.resize(slot + 1);
    }

    auto& point = m_points.at(slot);
    point.x = x;
    point.y = y;
    point.size = size;
  }

  // Invoke on main thread
  QMetaObject::invokeMethod(this, "update");
}

void QtTouchVisualizationWindow::paintEvent(QPaintEvent*)
{
  QPainter painter(this);
  painter.setPen(Qt::NoPen);
  painter.setBrush(QColor::fromRgb(0, 0, 0, 180));
  painter.setCompositionMode(QPainter::CompositionMode_Source);
  painter.drawRect(0, 0, width(), height());

  painter.setRenderHint(QPainter::Antialiasing, true);
  painter.setCompositionMode(QPainter::CompositionMode_SourceOver);

  int i = 0;
  std::lock_guard<decltype(m_pointsMutex)> lock(m_pointsMutex);
  for (auto const& point : m_points)
  {
    painter.setBrush(QColor::fromHsvF(static_cast<float> (i) / m_points.size(), 1.0f, 1.0f));
    double scaledX{point.x * width()}, scaledY{point.y * height()}, scaledSize{point.size * (width() + height()) / 2.0};
    painter.drawEllipse({scaledX, scaledY}, scaledSize, scaledSize);
    i++;
  }

  if (!m_fullscreen)
  {
    // Do not do in constructor because shell surface is not initialized yet there
    m_fullscreen = true;
    auto window = dynamic_cast<QtWaylandClient::QWaylandWindow*> (windowHandle()->handle());
    if (window)
    {
      auto shellSurface = dynamic_cast<QtWaylandClient::QWaylandXdgSurfaceV6*> (window->shellSurface());
      if (shellSurface)
      {
        wl_output* output{};
        /*
        if (qApp->screens().size() > 1)
        {
          // Show on second screen, does not work on mutter (bug #783709)
          output = dynamic_cast<QtWaylandClient::QWaylandScreen*> (qApp->screens()[1]->handle())->output();
        }
        */
        shellSurface->m_toplevel->set_fullscreen(output);
      }
      else
      {
        std::cerr << "Shell surface is not zxdg_surface_v6 - full screen might not work" << std::endl;
      }
    }
  }
}
