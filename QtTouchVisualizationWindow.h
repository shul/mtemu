// This file is part of mtemu. See toplevel LICENSE file for license.

#pragma once

#include <mutex>
#include <QWidget>
#include <vector>

#include "ITouchVisualization.h"

class QtTouchVisualizationWindow : public QWidget, public ITouchVisualization
{
  struct TouchPoint
  {
    double x{}, y{};
    double size{};
  };
  std::vector<TouchPoint> m_points;
  std::mutex m_pointsMutex;
  bool m_fullscreen{false};

public:
  QtTouchVisualizationWindow();

  void showHoverAt(unsigned int slot, double x, double y, double size) override;
  void paintEvent(QPaintEvent* event) override;
};