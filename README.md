# Multi-touch emulator for Linux

This is a simple converter from mouse input to the evdev multitouch protocol.
It is intended for testing simple multitouch applications without having access to a physical touch screen.

## Build

Requires Qt 5.9. Use qmake.

## Usage

`mtemu <scale factor> <mouse input device ...>`

Scale factor is the integer that the relative movement values of your mouse will be multiplicated before
they are added to the absolute touch position. 15 is a good value usually. The emulated touch plane has
a fixed size of 8192x8192.

You can specify any number of mouse input devices by their evdev device node path. Each of them will
represent one touch point. Use e.g. `evtest` to find out by which device node your mouse is represented.

You will have to have read-write access to both `/dev/uinput` and all device nodes you specify.

Move mouse to specify touch position. Left click to touch. Use scroll wheel to make touch area of contact smaller or bigger.

## Visualization

mtemu uses Qt on Wayland for displaying a full screen window that shows where your touches land. Qt Wayland support
still seems very experimental. Two monitors are required: One for showing the visualization and one for using
the application you want to test. It is recommended that they have the same aspect ratio.

## DE mapping

If you have multiple monitors, you might encounter problems with having your desktop environment map
the emulated touch screen to the right monitor.

### GNOME

For GNOME, you can set `/org/gnome/desktop/peripherals/touchscreen/0000:0000/display` to the EDID
manufacturer, product, serial number triplet of your monitor with `gsettings`:

Example: `gsettings set org.gnome.desktop.peripherals.touchscreen:/org/gnome/desktop/peripherals/touchscreens/0000:0000/ display "['GSM','IPS237','0x00001234']"`

You can get the EDID values from `gnome-rr-debug`.

### Weston

For weston, you can set the `WL_OUTPUT` udev key. See [here](https://wiki.tizen.org/IVI/Mapping_multiple_touchscreen_Wayland).