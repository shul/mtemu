// This file is part of mtemu. See toplevel LICENSE file for license.

#include "EvdevUInput.h"

#include <cassert>
#include <stdexcept>

EvdevUInput::EvdevUInput()
{
}

EvdevUInput::EvdevUInput(Evdev& evdev)
{
  libevdev_uinput* dev;
  EvdevCheckReturnValue(libevdev_uinput_create_from_device(evdev, LIBEVDEV_UINPUT_OPEN_MANAGED, &dev), "libevdev_uinput_create_from_device");
  assert(dev);
  m_dev.reset(dev, libevdev_uinput_destroy);
}

int EvdevUInput::fd()
{
  return libevdev_uinput_get_fd(cPtr());
}

std::string EvdevUInput::syspath()
{
  return libevdev_uinput_get_syspath(cPtr());
}

std::string EvdevUInput::devnode()
{
  return libevdev_uinput_get_devnode(cPtr());
}

void EvdevUInput::writeEvent(unsigned int type, unsigned int code, int value)
{
  EvdevCheckReturnValue(libevdev_uinput_write_event(cPtr(), type, code, value), "libevdev_uinput_write_event");
}

libevdev_uinput* EvdevUInput::cPtr()
{
  if (!m_dev)
  {
    throw std::logic_error("Operation invalid on empty EvdevUInput pointer");
  }
  return m_dev.get();
}

EvdevUInput::operator libevdev_uinput*()
{
  return m_dev.get();
}