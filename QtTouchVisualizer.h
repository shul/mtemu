// This file is part of mtemu. See toplevel LICENSE file for license.

#pragma once

#include <QApplication>

#include "ITouchVisualization.h"
#include "QtTouchVisualizationWindow.h"

class QtTouchVisualizer
{
  QApplication m_app;
  QtTouchVisualizationWindow m_window;

public:
  QtTouchVisualizer(int& argc, char**& argv);
  ITouchVisualization* getHandler();
  void run();
};